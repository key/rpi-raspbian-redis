FROM resin/rpi-raspbian:wheezy

RUN apt-get update
RUN apt-get install -y redis-server
RUN echo "vm.overcommit_memory = 1" >> /etc/sysctl.d/vm.conf

VOLUME ["/data"]

ADD redis.conf /etc/redis/redis.conf
EXPOSE 6379
CMD ["/usr/bin/redis-server", "/etc/redis/redis.conf"]
